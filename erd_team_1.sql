begin transaction;

CREATE TABLE Users
(
    ID int NOT NULL UNIQUE,
    Username varchar(30) NOT NULL UNIQUE,
    Name varchar(30) NOT NULL,
    Email varchar(50) NOT NULL,
    Password varchar(30) NOT NULL,

    PRIMARY KEY (ID),
);

CREATE TABLE Followers
(
    ID int NOT NULL,
    Follower_ID int,
    Following_ID int,

    PRIMARY KEY (USER_ID),
    CONSTRAINT id_of_follower FOREIGN KEY (Follower_ID) REFERENCES Users (ID),
    CONSTRAINT id_of_following_person FOREIGN KEY (Following_ID) REFERENCES Users (ID)
);

CREATE TABLE Posts
(
    ID int NOT NULL UNIQUE,
    Time timestamp NOT NULL,
    Text varchar(150),
    User_ID int NOT NULL,

    PRIMARY KEY (ID),
    CONSTRAINT id_of_post_author FOREIGN KEY (User_ID) REFERENCES Users (ID)
);

CREATE TABLE Comments
(
    ID int NOT NULL UNIQUE,
    Time timestamp NOT NULL,
    Text varchar(150),
    User_ID int NOT NULL,
    Post_ID bigint NOT NULL,

    PRIMARY KEY (ID),
    CONSTRAINT id_of_comment_author FOREIGN KEY (User_ID) REFERENCES Users (ID),
    CONSTRAINT id_of_commented_post FOREIGN KEY (Post_ID) REFERENCES Posts (ID),
);

CREATE TABLE Likes
(
    ID int NOT NULL UNIQUE,
    Time timestamp NOT NULL,
    Text varchar(150),
    User_ID int NOT NULL,
    Comment_ID bigint NOT NULL,
    Post_ID bigint NOT NULL,

    PRIMARY KEY (ID),
    CONSTRAINT user_id FOREIGN KEY (User_ID) REFERENCES Users (ID),
    CONSTRAINT id_of_liked_post FOREIGN KEY (Post_ID) REFERENCES Posts (ID),
    CONSTRAINT id_of_liked_comment FOREIGN KEY (Comment_ID) REFERENCES Comments (ID),
);

commit transaction ;
